const express = require("express");
const voucherHistoryMiddleware = require("../middlewares/voucherHistoryMiddleware");
const voucherHistoryRouter = express.Router();

voucherHistoryRouter.use(voucherHistoryMiddleware);

// import các hàm module controller
const { getAllVoucherHistory, getVoucherHistoryById, createVoucherHistory, updateVoucherHistoryById, deleteVoucherHistoryById } = require("../controllers/voucherHistoryController");

voucherHistoryRouter.get("/voucher-histories", getAllVoucherHistory);

voucherHistoryRouter.get("/voucher-histories/:historyId", getVoucherHistoryById);

voucherHistoryRouter.put("/voucher-histories/:historyId", updateVoucherHistoryById);

voucherHistoryRouter.post("/voucher-histories", createVoucherHistory);

voucherHistoryRouter.delete("/voucher-histories/:historyId", deleteVoucherHistoryById);

module.exports = voucherHistoryRouter;