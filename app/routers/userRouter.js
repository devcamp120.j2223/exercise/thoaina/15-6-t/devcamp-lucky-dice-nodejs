const express = require("express");
const userMiddleware = require("../middlewares/userMiddleware");
const userRouter = express.Router();

userRouter.use(userMiddleware);

// import các hàm module controller
const { getAllUser, getUserById, createUser, updateUserById, deleteUserById } = require("../controllers/userController");

userRouter.get("/users", getAllUser);

userRouter.get("/users/:userId", getUserById);

userRouter.put("/users/:userId", updateUserById);

userRouter.post("/users", createUser);

userRouter.delete("/users/:userId", deleteUserById);

module.exports = userRouter;