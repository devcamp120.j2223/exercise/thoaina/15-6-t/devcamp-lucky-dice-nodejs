const express = require("express");
const prizeMiddleware = require("../middlewares/prizeMiddleware");
const prizeRouter = express.Router();

prizeRouter.use(prizeMiddleware);

// import các hàm module controller
const { getAllPrize, getPrizeById, createPrize, updatePrizeById, deletePrizeById } = require("../controllers/prizeController");

prizeRouter.get("/prizes", getAllPrize);

prizeRouter.get("/prizes/:prizeId", getPrizeById);

prizeRouter.put("/prizes/:prizeId", updatePrizeById);

prizeRouter.post("/prizes", createPrize);

prizeRouter.delete("/prizes/:prizeId", deletePrizeById);

module.exports = prizeRouter;