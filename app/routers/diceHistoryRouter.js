const express = require("express");
const diceHistoryMiddleware = require("../middlewares/diceHistoryMiddleware");
const diceHistoryRouter = express.Router();

diceHistoryRouter.use(diceHistoryMiddleware);

// import các hàm module controller
const { getAllDiceHistory, getDiceHistoryById, createDiceHistory, updateDiceHistoryById, deleteDiceHistoryById } = require("../controllers/diceHistoryController");

diceHistoryRouter.get("/dice-histories", getAllDiceHistory);

diceHistoryRouter.get("/dice-histories/:diceHistoryId", getDiceHistoryById);

diceHistoryRouter.put("/dice-histories/:diceHistoryId", updateDiceHistoryById);

diceHistoryRouter.post("/dice-histories", createDiceHistory);

diceHistoryRouter.delete("/dice-histories/:diceHistoryId", deleteDiceHistoryById);

module.exports = diceHistoryRouter;