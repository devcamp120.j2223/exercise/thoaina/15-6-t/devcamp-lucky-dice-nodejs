// import prize model
const { mongoose } = require("mongoose");
const prizeModel = require("../models/prizeModel");

// get all prizes
const getAllPrize = (req, res) => {
    prizeModel.find((error, data) => {
        if (error) {
            res.status(500).json({
                message: `Internal server error: ${error.message}`,
            });
        }
        else {
            res.status(200).json({
                ...data
            });
        }
    })
};

const getPrizeById = (req, res) => {
    let id = req.params.prizeId;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        prizeModel.findById(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

// tạo mới prize
const createPrize = (req, res) => {
    // B1: thu thập dữ liệu
    let body = req.body;

    // B2: Kiểm tra dữ liệu
    if (!body.name) {
        res.status(400).json({
            message: "name is require!",
        })
    }
    else {
        // B3: Thực hiện thao tác nghiệp vụ
        let prize = {
            _prizeId: mongoose.Types.ObjectId(),
            name: body.name,
        };
        prizeModel.create(prize, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(201).json({
                    data
                });
            }
        });
    }
};

const updatePrizeById = (req, res) => {
    let id = req.params.prizeId;
    let body = req.body;

    if (!body.name) {
        res.status(400).json({
            message: "name is require!",
        })
    }
    else {
        let prize = {
            name: body.name,
        };
        prizeModel.findByIdAndUpdate(id, prize, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

const deletePrizeById = (req, res) => {
    let id = req.params.prizeId;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        prizeModel.findByIdAndDelete(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(204).json({
                    data
                });
            }
        })
    }
};
// export các hàm
module.exports = { getAllPrize, getPrizeById, createPrize, updatePrizeById, deletePrizeById };