// import diceHistory model
const { mongoose } = require("mongoose");
const diceHistoryModel = require("../models/diceHistoryModel");

// get all dicehisory
const getAllDiceHistory = (req, res) => {
    diceHistoryModel.find((error, data) => {
        if (error) {
            res.status(500).json({
                message: `Internal server error: ${error.message}`,
            });
        }
        else {
            res.status(200).json({
                ...data
            });
        }
    })
};

const getDiceHistoryById = (req, res) => {
    let id = req.params.diceHistoryId;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        diceHistoryModel.findById(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

// tạo mới Dice History
const createDiceHistory = (req, res) => {
    // B1: thu thập dữ liệu
    let body = req.body;

    // B2: Kiểm tra dữ liệu
    if (isNaN(body.dice) && body.dice <= 0) {
        res.status(400).json({
            message: "dice is invalid!",
        })
    }
    else {
        // B3: Thực hiện thao tác nghiệp vụ
        let diceHistory = {
            _diceHistoryId: mongoose.Types.ObjectId(),
            dice: body.dice,
        };
        diceHistoryModel.create(diceHistory, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(201).json({
                    data
                });
            }
        });
    }
};

const updateDiceHistoryById = (req, res) => {
    let id = req.params.diceHistoryId;
    let body = req.body;

    if (isNaN(body.dice) && body.dice <= 0) {
        res.status(400).json({
            message: "dice is invalid!",
        })
    }
    else {
        let diceHistory = {
            dice: body.dice,
        };
        diceHistoryModel.findByIdAndUpdate(id, diceHistory, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

const deleteDiceHistoryById = (req, res) => {
    let id = req.params.diceHistoryId;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        diceHistoryModel.findByIdAndDelete(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(204).json({
                    data
                });
            }
        })
    }
};
// export các hàm
module.exports = { getAllDiceHistory, getDiceHistoryById, createDiceHistory, updateDiceHistoryById, deleteDiceHistoryById };