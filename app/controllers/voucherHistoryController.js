// import voucher history model
const { mongoose } = require("mongoose");
const voucherHistoryModel = require("../models/voucherHistoryModel");

// get all voucher histories
const getAllVoucherHistory = (req, res) => {
    voucherHistoryModel.find((error, data) => {
        if (error) {
            res.status(500).json({
                message: `Internal server error: ${error.message}`,
            });
        }
        else {
            res.status(200).json({
                ...data
            });
        }
    })
};

const getVoucherHistoryById = (req, res) => {
    let id = req.params.historyId;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        voucherHistoryModel.findById(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

// tạo mới voucher history
const createVoucherHistory = (req, res) => {
    // B1: thu thập dữ liệu
    let body = req.body;

    // B2: Kiểm tra dữ liệu
    if (!body.user) {
        res.status(400).json({
            message: "user is require!",
        })
    }
    else if (!body.voucher) {
        res.status(400).json({
            message: "voucher is require!",
        })
    }
    else {
        // B3: Thực hiện thao tác nghiệp vụ
        let voucherHistory = {
            _id: mongoose.Types.ObjectId(),
            user: body.user,
            voucher: body.voucher,
        };
        voucherHistoryModel.create(voucherHistory, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(201).json({
                    data
                });
            }
        });
    }
};

const updateVoucherHistoryById = (req, res) => {
    let id = req.params.historyId;
    let body = req.body;

    if (!body.user) {
        res.status(400).json({
            message: "user is require!",
        })
    }
    else if (!body.voucher) {
        res.status(400).json({
            message: "voucher is require!",
        })
    }
    else {
        let voucherHistory = {
            user: body.user,
            voucher: body.voucher,
        };
        voucherHistoryModel.findByIdAndUpdate(id, voucherHistory, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

const deleteVoucherHistoryById = (req, res) => {
    let id = req.params.historyId;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        voucherHistoryModel.findByIdAndDelete(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(204).json({
                    data
                });
            }
        })
    }
};
// export các hàm
module.exports = { getAllVoucherHistory, getVoucherHistoryById, createVoucherHistory, updateVoucherHistoryById, deleteVoucherHistoryById };