// import prizeHistory model
const { mongoose } = require("mongoose");
const prizeHistoryModel = require("../models/prizeHistoryModel");

const getAllPrizeHistory = (req, res) => {
    prizeHistoryModel.find((error, data) => {
        if (error) {
            res.status(500).json({
                message: `Internal server error: ${error.message}`,
            });
        }
        else {
            res.status(200).json({
                ...data
            });
        }
    })
};

const getPrizeHistoryById = (req, res) => {
    let id = req.params.historyId;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        prizeHistoryModel.findById(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

const createPrizeHistory = (req, res) => {
    // B1: thu thập dữ liệu
    let body = req.body;

    // B2: Kiểm tra dữ liệu
    if (!body.user) {
        res.status(400).json({
            message: "user is invalid!",
        })
    }
    if (!body.prize) {
        res.status(400).json({
            message: "prize is invalid!",
        })
    }
    else {
        // B3: Thực hiện thao tác nghiệp vụ
        let prizeHistory = {
            _historyId: mongoose.Types.ObjectId(),
            user: body.user,
            prize: body.prize,
        };
        prizeHistoryModel.create(prizeHistory, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(201).json({
                    data
                });
            }
        });
    }
};

const updatePrizeHistoryById = (req, res) => {
    let id = req.params.historyId;
    let body = req.body;

    if (!body.user) {
        res.status(400).json({
            message: "user is invalid!",
        })
    }
    if (!body.prize) {
        res.status(400).json({
            message: "prize is invalid!",
        })
    }
    else {
        let prizeHistory = {
            user: body.user,
            prize: body.prize,
        };
        prizeHistoryModel.findByIdAndUpdate(id, prizeHistory, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

const deletePrizeHistoryById = (req, res) => {
    let id = req.params.historyId;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        prizeHistoryModel.findByIdAndDelete(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(204).json({
                    data
                });
            }
        })
    }
};
// export các hàm
module.exports = { getAllPrizeHistory, getPrizeHistoryById, createPrizeHistory, updatePrizeHistoryById, deletePrizeHistoryById };