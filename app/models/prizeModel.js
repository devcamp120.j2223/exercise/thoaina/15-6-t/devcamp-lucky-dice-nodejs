// Khai báo
const mongoose = require("mongoose");

// Khai báo thư viện Schema
const Schema = mongoose.Schema;

// Tạo đối tượng Schema tương ứng với Collection
const prizeSchema = new Schema({
    _prizeId: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    name: {
        type: String,
        require: true,
    },
    description: {
        type: String,
        require: false,
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    },
});

// export 
module.exports = mongoose.model("Prize", prizeSchema);