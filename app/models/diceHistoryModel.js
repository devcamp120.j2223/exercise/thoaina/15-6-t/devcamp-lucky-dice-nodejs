// Khai báo
const mongoose = require("mongoose");

// Khai báo thư viện Schema
const Schema = mongoose.Schema;

// Tạo đối tượng Schema tương ứng với Collection
const diceHistorySchema = new Schema({
    _diceHistoryId: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    username: [
        {
            type: mongoose.Types.ObjectId,
            ref: "User"
        }
    ],
    dice: {
        type: Number,
        require: true,
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    },
});

// export 
module.exports = mongoose.model("DiceHistory", diceHistorySchema);