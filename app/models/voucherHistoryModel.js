// Khai báo
const mongoose = require("mongoose");

// Khai báo thư viện Schema
const Schema = mongoose.Schema;

// Tạo đối tượng Schema tương ứng với Collection
const voucherHistorySchema = new Schema({
    _voucherHistoryId: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    user: [
        {
            type: mongoose.Types.ObjectId,
            ref: "User",
            required: true
        }
    ],
    voucher: [
        {
            type: mongoose.Types.ObjectId,
            ref: "Voucher",
            required: true
        }
    ],
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    },
});

// export 
module.exports = mongoose.model("VoucherHistory", voucherHistorySchema);