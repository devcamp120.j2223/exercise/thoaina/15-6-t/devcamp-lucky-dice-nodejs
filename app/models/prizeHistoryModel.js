// Khai báo
const mongoose = require("mongoose");

// Khai báo thư viện Schema
const Schema = mongoose.Schema;

// Tạo đối tượng Schema tương ứng với Collection
const prizeHistorySchema = new Schema({
    _prizeHistoryId: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    user: [
        {
            type: mongoose.Types.ObjectId,
            ref: "User",
            required: true
        }
    ],
    prize: [
        {
            type: mongoose.Types.ObjectId,
            ref: "Voucher",
            required: true
        }
    ],
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    },
});

// export 
module.exports = mongoose.model("PrizeHistory", prizeHistorySchema);