// Khai báo
const mongoose = require("mongoose");

// Khai báo thư viện Schema
const Schema = mongoose.Schema;

// Tạo đối tượng Schema tương ứng với Collection
const userSchema = new Schema({
    _userId: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    username: {
        type: String,
        unique: true,
        require: true,
    },
    firstname: {
        type: String,
        unique: true,
        require: true,
    },
    lastname: {
        type: String,
        unique: true,
        require: true,
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    },
});

// export 
module.exports = mongoose.model("User", userSchema);